package com.koval.model;

import java.util.ArrayList;

public interface Model {
    ArrayList<Door> generateDoors();
    ArrayList<Door>getDoors();
}