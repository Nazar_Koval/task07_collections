package com.koval.controller;

import com.koval.model.*;
import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ControllerImplemented implements Controller{

    private static Logger control_logger = LogManager.getLogger(ControllerImplemented.class);
    private Model model;
    private ArrayList<Door>doors;
    private List<Integer>death;
    private List<Integer>sequence;
    private boolean[] used = new boolean[33];

    public ControllerImplemented(){
        this.model = new Logic();
        this.doors = this.model.getDoors();
        this.sequence = new ArrayList<>();
        this.death = new ArrayList<>();
        for (int i = 0; i <33 ; i++) {
            this.used[i] = false;
        }
    }

    @Override
    public void deathDoors(ArrayList<Door> doors, List<Integer>death, int index) {

        if(!this.used[index] && index >= 1 && index <= 10){
            this.used[index] = true;

            if( doors.get(index-1).name.equals("Dragon")) {

                if (doors.get(index - 1).power > 25) {
                    this.death.add(index);
                }
            }

            deathDoors(this.doors, this.death, index - 1);

            deathDoors(this.doors, this.death, index + 1);
        }

    }

    @Override
    public void winningPath(ArrayList<Door> doors, int power) {

        for (int i = 0; i <doors.size() ; i++) {

            if(doors.get(i).name.equals("Artifact")) {
                power += doors.get(i).power;
                sequence.add(doors.get(i).index);
            }

        }

        for (int i = 0; i <doors.size() ; i++) {

            if(doors.get(i).name.equals("Dragon")){
                power -= doors.get(i).power;
                sequence.add(doors.get(i).index);

                if(power < 0){
                    control_logger.trace("There is no chance to win.\n\n");
                    sequence.clear();
                    break;
                }

            }

        }

    }

    @Override
    public ArrayList<Door> getDoors() {
        return this.doors;
    }

    @Override
    public List<Integer> getDeathDoors() {
        deathDoors(this.doors, this.death,5);
        return this.death;
    }

    @Override
    public List<Integer> getWinningPath() {
        winningPath(this.doors,25);
        return this.sequence;
    }

}
