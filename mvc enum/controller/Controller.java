package com.koval.controller;

import com.koval.model.*;
import java.util.ArrayList;
import java.util.List;

public interface Controller {

    void deathDoors(ArrayList<Door>doors, List<Integer>death, int index);
    void winningPath(ArrayList<Door>doors, int power);
    ArrayList<Door>getDoors();
    List<Integer>getWinningPath();
    List<Integer> getDeathDoors();

}
