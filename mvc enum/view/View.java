package com.koval.view;

import com.koval.controller.*;
import com.koval.model.*;
import java.util.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class View{

    private static Logger view_logger = LogManager.getLogger(View.class);
    private static Controller controller;
    private Scanner input;
    private Map<String,String> commands;
    private static ArrayList<Door>doors;

    enum Menu implements Executable{
        FIRST{
            @Override
            public void execute() {
                doors = controller.getDoors();
                view_logger.trace("List of the doors: \n");
                for (Door d:doors) {
                    view_logger.trace(d.toString() + "\n");
                }
                view_logger.trace("\n");
            }
        },
        SECOND{
            @Override
            public void execute() {
                List<Integer> death = controller.getDeathDoors();
                view_logger.trace("Death waits for you beyond these doors: \n");
                for(Integer i: death){
                    view_logger.trace(doors.get(i-1).toString() + "\n");
                }
            }
        },
        THIRD{
            @Override
            public void execute() {
                List<Integer> sequence = controller.getWinningPath();
                if(sequence.isEmpty())return;
                view_logger.trace("You need to complete this path to win the game: \n");
                for (Integer i: sequence) {
                    view_logger.trace(i + "->");
                }
                view_logger.trace("|Endpoint|");
                view_logger.trace("\n\n");
            }
        },
        FOURTH{
            @Override
            public void execute() {
                view_logger.trace("Bye!");
            }
        }
    }

    public View(){
        controller = new ControllerImplemented();
        input = new Scanner(System.in);

        commands = new HashMap<>();
        commands.put("1", "1 - Show all the doors");
        commands.put("2", "2 - Show all the doors with dragon that will kill you (excluding artifacts)");
        commands.put("3", "3 - Calculate the winning path");
        commands.put("4", "4 - Quit");

        interact();
    }

    private void showMenu(){
        view_logger.trace("~~~Game menu~~~\n");

        for (String str: commands.values()) {
            System.out.println(str);
        }

        view_logger.trace("~~~Game menu~~~\n\n");
    }

    private void interact(){
        int key;
        do {
            showMenu();
            view_logger.trace("Input an option: ");
            key = input.nextInt();
            Menu.values()[key-1].execute();
        }while (key != 4);

    }
}
