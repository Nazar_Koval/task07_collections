package com.koval.view;

@FunctionalInterface
public interface Executable {
    void execute();
}
