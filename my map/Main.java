package com.koval;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.Comparator;

public class Main {
    private static Logger logger = LogManager.getLogger(Main.class);

    public static void main(String[] args) {
        MyMap<Integer,Integer>myMap = new MyMap<>(new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return o1.compareTo(o2);
            }
        });
        myMap.put(1,10);
        myMap.put(2,20);
        myMap.put(3,30);
        if(myMap.containsKey(3)){
            logger.trace("Contains\n");
        }
        logger.trace(myMap.size() + "\n");
        myMap.remove(2);
        logger.trace(myMap.get(2)+"\n");
        myMap.put(1,null);
        logger.trace(myMap.get(1)+"\n");
        myMap.clear();
        if(myMap.isEmpty())
            logger.trace("End of the test");
    }
}
