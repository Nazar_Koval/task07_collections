package com.koval.model;

import java.util.*;

/**
 * Helping class that is a prototype of the model
 */
public class Remedy implements Comparator<Remedy>, Comparable<Remedy> {

    private String name;
    private Integer price;
    private Integer amount;
    private String belonging;
    private List<Remedy>remedies;
    private Integer id;

    private Remedy(String name, Integer price, Integer amount, String belonging, Integer id){
        this.name = name;
        this.price = price;
        this.amount = amount;
        this.belonging = belonging;
        this.id = id;
    }

    public Remedy(){
        remedies = generateGoods();
        sortByPrice();
    }

    private String[] belongings = {"Window cleaner" , "Bathroom cleaner" ,
            "Washing Powder", "Washing tablet" , "Floor cleaner", "Wood cleaner", "Cleaning spray", "Washing gel"};

    private String[] names = {"Gala", "Domestos", "Cleaning Pro", "House master", "Helper", "Mr.Proper", "Watson`s"};

    /**
     * Method that generates the list of all the products available
     * @return list of the products
     */
    private List<Remedy> generateGoods(){
        Random random = new Random();
        List<Remedy>available = new LinkedList<Remedy>();

        for (int i = 0; i < 20 ; i++) {
            Remedy remedy = new Remedy(names[random.nextInt(names.length)],random.nextInt(501),
                    random.nextInt(151),belongings[random.nextInt(belongings.length)], i+1);
            available.add(remedy);
        }

        return available;
    }

    /**
     * Getter method
     * @return name of the product
     */
    public String getName() {
        return name;
    }

    /**
     * Getter method
     * @return amount of the product
     */
    public Integer getAmount(){
        return amount;
    }

    /**
     * Getter method
     * @return id of the product
     */
    public Integer getId() {
        return id;
    }

    /**
     * Getter method
     * @return list of the products
     */
    public List<Remedy> getRemedies(){
        return remedies;
    }

    /**
     * Method that sort the list of the goods by prices
     */
    private void sortByPrice(){
        Collections.sort(remedies,Remedy.priceComparator);
    }

    /**
     * Method that counts all different brands available
     * @return set of unique brand names
     */
    public Integer getAllBrands(){
        Set<String> unique = new HashSet<String>();

        for (Remedy element: remedies) {
            unique.add(element.getName());
        }

        return unique.size();
    }

    /**
     * Method that counts the total amount of the products in the store
     * @return total amount of the products available
     */
    public Integer getTotalAmount(){
        Integer sum = 0;

        for (Remedy element: remedies) {
            sum += element.getAmount();
        }

        return sum;
    }

    /**
     * Overridden method that prints all the information about a particular product
     * @return string with all product`s information
     */
    @Override
    public  String toString(){
        return "BRAND: " + this.name + "\n" + "PRICE: " + this.price + " $" +"\n" + "AMOUNT: " +
                this.amount + " c.u." +"\n" + "BELONGING: " + this.belonging + "\n" + "ID: " + this.id + "\n\n" ;
    }

    @Override
    public int compareTo(Remedy o) {
        return 0;
    }

    @Override
    public int compare(Remedy o1, Remedy o2) {
        return 0;
    }

    /**
     * Comparator that sorts the list of the goods by price
     */
    private static Comparator<Remedy> priceComparator = new Comparator<Remedy>() {
        @Override
        public int compare(Remedy o1, Remedy o2) {
            return o1.price.compareTo(o2.price);
        }
    };
}