package com.koval.view;

/**
 * Functional interface for printing menu options
 */
@FunctionalInterface
public interface Printable {
    void print();
}
