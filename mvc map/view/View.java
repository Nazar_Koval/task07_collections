package com.koval.view;

import com.koval.controller.*;
import com.koval.model.Remedy;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

/**
 * View class that interacts with user with the help of controller
 */
public class View {

    private Controller controller;
    private Scanner input;
    private Map<String,String> commands;
    private Map<String, Printable> methods;
    private List<Remedy>bucket;
    private List<Remedy> remedies;

    /**
     * Main constructor of the class
     */
    public View(){
        controller = new ControllerImplemented();
        input = new Scanner(System.in);
        remedies = controller.getRemedies();
        bucket = controller.getBucket();

        commands = new HashMap<>();
        commands.put("1", "1 - Show the list of the goods.");
        commands.put("2", "2 - Total amount of goods and brands.");
        commands.put("3", "3 - Add chosen product to bucket.");
        commands.put("4", "4 - Delete chosen product from the bucket.");
        commands.put("5", "5 - Show customer`s bucket.");
        commands.put("Exit", "Press Q to exit from the menu");

        methods = new HashMap<>();
        methods.put("1", this::firstMethod);
        methods.put("2", this::secondMethod);
        methods.put("3", this::thirdMethod);
        methods.put("4", this::fourthMethod);
        methods.put("5", this::fifthMethod);
    }

        /**
        * Method that outputs the list of all the products.
        */
        public void firstMethod(){

        for (Remedy tmp: remedies) {
            System.out.print(tmp.toString());
        }

        System.out.println("\n");
        }

        /**
        * Method that outputs number of different product brands and amount of all the products.
        */
        public void secondMethod(){
        System.out.println(controller.getStatistics());
        System.out.println("\n");
        }

        /**
        * Method that enables user to add a chosen product to the user`s bucket.
        */
        public void thirdMethod() {
            System.out.println("Input the id of the product you want to add");
            int id = input.nextInt();

            if (id <= 0 || id > remedies.size()) {
                System.err.println("Wrong id input!");
            } else {
                for(Remedy rm : remedies){

                    if(rm.getId() == id){
                        controller.addToBucket(rm);
                        break;
                    }

                }

                System.out.println("Done!");
            }

        }

        /**
        * Method that enables user to delete a chosen from user`s bucket.
        */
        public void fourthMethod() {
            System.out.println("Input the id of the product you want to delete");
            int id = input.nextInt();

            if (id <= 0 || id > remedies.size()) {
                System.err.println("Wrong id input!");
            } else {
                for (Remedy rm:remedies) {

                    if(rm.getId() == id){

                        if(bucket.contains(rm)) {
                            controller.deleteFromBucket(rm);
                            break;
                        }else {
                            System.err.println("There is no such product in your bucket");
                        }

                    }

                }

                System.out.println("Done!");
            }
        }

        /**
        * Method that shows off user`s bucket taking into account all the previous changes.
        */
        public void fifthMethod(){

            if(!bucket.isEmpty()) {
                System.out.println("Your bucket\n");
                controller.showBucket();
            }else {
                System.err.println("Your bucket is empty");
            }

        }

        /**
        * Method that outputs products` menu.
        */
        public void showMenu(){
            System.out.println("~~~Goods` menu~~~");

            for (String str: commands.values()) {
                System.out.println(str);
            }

            System.out.println("~~~Goods` menu~~~\n");
        }

        /**
        * Interactive method that outputs products` menu and interact with user while program is being run.
        */
        public void interact(){
            String key;

            do {

                showMenu();
                System.out.println("Input an option");
                key = input.nextLine().toUpperCase();

                if(methods.containsKey(key)) {

                    try {
                        methods.get(key).print();
                    } catch (Exception exc) {
                    }

                }else {
                    System.err.println("Wrong input! Try again");
                }

            }while (!key.equals("Q"));

            System.out.println("Thanks for using this program. Bye!");
        }
    }
